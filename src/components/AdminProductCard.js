import { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

export default function AdminProductCard({ product, onUpdate }) {
  const { _id, name, description, price, isActive, imageUrl} = product;
  const [showModal, setShowModal] = useState(false);
  const [editedProduct, setEditedProduct] = useState(product);

  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setEditedProduct({ ...editedProduct, [name]: value });
  };

  const handleUpdateProduct = async () => {
    console.log('Updating product...');
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
        method: 'PUT',
        headers: {  
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(editedProduct),
      });
    } catch (error) {
      console.error('Error updating product:', error);
    }
    console.log('Product updated successfully!');
    onUpdate();
    handleCloseModal();
  };

  return (
    <>
      <tr key={_id}>
        <td>{name}</td>
        <td>{description}</td>
        <td>{price}</td>
        <td>{imageUrl}</td>
        <td>{isActive ? 'Available' : 'Unavailable'}</td>
        <td>
          <Button variant="primary" onClick={handleShowModal}>
            Modify
          </Button>
        </td>
      </tr>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <label>
            Name:
            <br></br>
            <input type="text" name="name" value={editedProduct.name} onChange={handleInputChange} style={{ paddingRight: '10px' }} />
          </label>
          <br></br>
          <br></br>
          <label>
            Description:
            <textarea name="description" value={editedProduct.description} onChange={handleInputChange} rows="10" cols="50" />
          </label>
          <br></br>
          <br></br>
          <label>
            Price:
            <br></br>
            <input type="number" name="price" value={editedProduct.price} onChange={handleInputChange} style={{ paddingRight: '10px' }} />
          </label>
          <br></br>
          <br></br>
          <label>
            Image File Name:
            <br></br>
            <textarea name="imageUrl" value={editedProduct.imageUrl} onChange={handleInputChange} rows="3" cols="50" />
          </label>
          <br></br>
          <br></br>
          <label>
            Available:
            <select name="isActive" value={editedProduct.isActive} onChange={handleInputChange}>
              <option value="true">Yes</option>
              <option value="false">No</option>
            </select>
          </label>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleUpdateProduct}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

