// UserCard.js
import { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';



export default function UserCard({ user, onUpdate }) {
  const { _id, firstName, lastName, email, isAdmin, mobileNo } = user;
  const [showModal, setShowModal] = useState(false);
  const [editedUser, setEditedUser] = useState(user);

  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setEditedUser({ ...editedUser, [name]: value });
  };


  const handleUpdateUser= async () => {
    console.log('Updating user...');
    try {
      console.log("update")
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/${_id}`, {
        method: 'PATCH',
        headers: {  
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(editedUser),

      });;
    } catch (error) {
      console.error('Error updating user:', error);
    }
    console.log('Product updated successfully!');
    onUpdate();
    handleCloseModal();
  };


  return (
    <>
      <tr key={_id}>
        <td><strong>{firstName} {lastName}</strong> 
        <br></br>
        <br></br>
        {isAdmin ? 'Admin' : 'Non-Admin'}</td>
        <td>{mobileNo}</td>
        <td>{email}</td>  
        <td>
          <Button variant="primary" onClick={handleShowModal}>
            Modify
          </Button>
        </td>
      </tr>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Edit User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <label style={{padding: '10px'}}>
            First Name:
            <br></br>
            <input type="text" name="firstName" value={editedUser.firstName} onChange={handleInputChange} style={{ width: '228%' }}/>
          </label>
          <br></br>
          <label style={{padding: '10px'}}>
            Last Name:
            <br></br>
            <input type="text" name="lastName" value={editedUser.lastName} onChange={handleInputChange} style={{ width: '228%' }}/>
          </label>
          <br></br>
          <label style={{padding: '10px'}}>
            Mobile Number:
            <br></br>
            <input type="text" name="mobileNo" value={editedUser.mobileNo} onChange={handleInputChange} />
          </label>
          <br></br>
          <label style={{padding: '10px'}}>
            Email Address:
            <br></br>
            <input type="email" name="email" value={editedUser.email} onChange={handleInputChange} style={{ width: '228%' }}/>
          </label>
          <br></br>
          <label style={{padding: '10px'}}>
            Admin:
            <select name="isAdmin" value={editedUser.isAdmin} onChange={handleInputChange}>
              <option value={true}>Yes</option>
              <option value={false}>No</option>
            </select>
          </label>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleUpdateUser}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}