import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';

export default function Footer() {
  return (
    <footer>
      <div className="social-media-icons text-center">
        <a href="https://www.facebook.com/yourcompanyname"><FontAwesomeIcon icon={faFacebook} size="2x" className="mr-3 text-black" /></a>
        <a href="https://www.twitter.com/yourcompanyname"><FontAwesomeIcon icon={faTwitter} size="2x" className="mr-3 text-black" /></a>
        <a href="https://www.instagram.com/yourcompanyname"><FontAwesomeIcon icon={faInstagram} size="2x" className="text-black" /></a>
      </div>
      <p className="text-center mt-3">&copy; 2023 SneakPick. All Rights Reserved.</p>
    </footer>
  );
}

