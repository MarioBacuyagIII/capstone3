import { useEffect, useState } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { Card, Button } from 'react-bootstrap';
import './Hero.css';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Container, Row, Col } from 'react-bootstrap';
import './Highlights.css';
import picLaptop from "../laptop.jpg"
import picCellphone from "../cellphone.jpg"

export default function Highlights() {
  return (
    <Container>
      <h3 className="mb-4 text-left" style={{ color: 'black', textShadow: '0 0 10px darkred' }}>What's hot?</h3>
      <Row>
        <Col xs={12} md={6} className="pb-3 pb-md-0">
          <div className="image-container">
            <img src={picLaptop} className="img-fluid" alt="First image" style={{ maxHeight: 900 }} />
            <div className="button-container">
              <Button className='btn btn-outline-light btn-lg btn-pulse'>Macbook Pro</Button>
            </div>
          </div>
        </Col>
        <Col xs={12} md={6} className="pb-3 pb-md-0">
          <div className="image-container">
            <img src={picCellphone} className="img-fluid" alt="Second image" style={{ maxHeight: 900 }} />
            <div className="button-container">
              <Button className='btn btn-outline-light btn-lg btn-pulse'>Iphone 14</Button>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
