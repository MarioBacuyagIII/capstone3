import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import './ProductCard.css'

export default function ProductCard({ productProp }) {
  const { _id, name, price, imageUrl } = productProp;

  // Format price to currency
  const formattedPrice = Number(price).toLocaleString('en-PH', { style: 'currency', currency: 'PHP' });

  return (
    <Link to={`/products/${_id}`} style={{ textDecoration: 'none' }}>
      <Card className="border-0" style={{ height: '320px', cursor: 'pointer', overflow: 'hidden' }}>
        <div className="zoom-in">
          <Card.Img src={imageUrl} style={{ height: '220px', objectFit: 'contain' }} />
        </div>
        <Card.Body className="text-center">
          <Card.Title style={{ color: 'darkblue', fontSize: '1.4rem' }}>{name}</Card.Title>
          <div >
            <Card.Subtitle className="text-center" style={{ fontSize: '.9rem', color: 'black' }}>Price: {formattedPrice}</Card.Subtitle>
          </div>
        </Card.Body>
      </Card>
    </Link>
  );
}
