import { Container } from 'react-bootstrap';

export default function About() {
  return (
    <Container fluid className ="vh-50" style={{ 
      color: 'black',
      padding: '50px',
      fontFamily: 'sans-serif',
      fontSize: '1.2rem'
    }}>
      <div>
        <h1 style={{color: '#000'}}>TechTonic - Unleash the Power of the Latest Tech</h1>
        <br></br>
        <p style={{textAlign: 'justify'}}>TechTonic is an online store that specializes in providing customers with the latest technology products. From laptops and cellphones to tablets and gaming accessories, TechTonic offers an extensive catalog of products from some of the top technology brands in the market. The company is committed to providing customers with a seamless online shopping experience, with a user-friendly website and reliable shipping.</p>
        <p style={{textAlign: 'justify'}}>One of the key features of TechTonic is its commitment to customer service. The company understands the importance of delivering an exceptional customer experience and offers a range of support options for customers, including phone, email, and live chat. In addition, the website provides detailed product descriptions and customer reviews, allowing customers to make informed purchasing decisions.</p>
        <p style={{textAlign: 'justify'}}>Overall, TechTonic is an excellent choice for anyone looking to purchase the latest technology products online. With a wide range of products, a user-friendly website, and excellent customer service, TechTonic is committed to providing customers with an exceptional online shopping experience. Whether you're in the market for a new laptop, cellphone, or gaming accessory, TechTonic is the go-to destination for all your tech needs.</p>
        <br></br>
        <p>Don't miss out on the hottest Tech releases – shop at Tech Tonic today!</p>
      </div>
    </Container>
  );
}
