import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import HeroImage from '../components/Hero';
import Highlights from '../components/Highlights';
import { useContext} from 'react'
import UserContext from '../UserContext';
import { Form, Button } from 'react-bootstrap';
import About from '../components/About'
import Footer from '../components/Footer';
import { Link } from 'react-router-dom';

export default function Home() {

  const { user } = useContext(UserContext);

  return (
    <Container>
      {user.isAdmin ? (
      	<>
        <div className="text-center">
          <h3 style={{fontSize: '2rem'}}>Admin Functions</h3>
          <Link to='/products/add'>
            <Button variant="outline-secondary" size="lg" className="mx-3">Add Product</Button>
          </Link>
          <Link to='products/all'>
            <Button variant="outline-secondary" size="lg" className="mx-3">All Products</Button>
          </Link>
          <Link to='/users'>
            <Button variant="outline-secondary" size="lg" className="mx-3">Users</Button>
          </Link>
        </div>
        </>
      ) : (
        <>
          <HeroImage/>
          <br/>
          <Highlights/>
          <br/>
          <br/>
          <About/>
          <Footer/>
        </>
      )}
    </Container>
  );
}
