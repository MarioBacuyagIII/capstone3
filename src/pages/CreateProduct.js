import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams} from 'react-router-dom';


export default function CreateProduct(){

  const { user } = useContext(UserContext);
  const { productName } = useParams();
	// State hooks to store the values of the input fields
	const [name, setName] = useState('');
	const [brand, setBrand] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState("0");
	const [imageUrl, setImageUrl] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfullt binded
	console.log(name);
	console.log(description);
	console.log(price);

/*	useEffect(() => {

		if (name !== '' && description !== '' && price !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price]);*/



	// Function to add product
function addProduct(e) {
  e.preventDefault();

  // Verify user is admin
  fetch(`${process.env.REACT_APP_API_URL}/products`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
      body: JSON.stringify({
      name: name
      })

  })
  .then(res => res.json())
    .then(data => {
      if (data === true) {
        Swal.fire({
          title: "Duplicate product found!",
          icon: "error",
          text: "Please enter a new product name."
        })
      } else {
        // Add new product
        fetch(`${process.env.REACT_APP_API_URL}/products`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            name: name,
            brand: brand,
            description: description,
            price: price,
            imageUrl: imageUrl
          })
        })
        .then(res => res.json())
        .then(data => {
          if(data === false){
            Swal.fire({
              title: "Yahoo!",
              icon: "success",
              text: "Product added!"
            })

            setName('');
            setDescription('');
            setPrice('');
            setImageUrl('')
          } else {
            Swal.fire({
              title: "Something went wrong",
              icon: "error",
              text: "Please try again."
            })
          }
        })
      }
    })
}
	// Validation to enable submit button when all input fields are populated and both passwords match


	return	(
		<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
		  <div style={{ display: 'flex', flexDirection: 'row', maxWidth: '800px' }}>
		    <div style={{ flex: 1 }}>
		      <Form onSubmit={(e) => {
  e.preventDefault();
  addProduct(e);
}}>
  <h1>Add Product</h1>
  <Row>
    <Col md={6}>
      <Form.Group controlId="productName">
        <Form.Label>Product Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Product Name"
          required
          value={name}
          onChange={e => setName(e.target.value)}
        />
        <Form.Text className="text-muted">Please enter product name</Form.Text>
      </Form.Group>
      <br></br>
      <Form.Group controlId="productBrand">
        <Form.Label>Brand Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Brand Name"
          required
          value={brand}
          onChange={e => setBrand(e.target.value)}
        />
        <Form.Text className="text-muted">Please enter brand name</Form.Text>
      </Form.Group>
      <br></br>
      <Form.Group controlId="productPrice">
        <Form.Label>Product Price</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter product price"
          required
          value={price}
          onChange={e => setPrice(e.target.value)}
        />
        <Form.Text className="text-muted">Please enter product price</Form.Text>
      </Form.Group>
      <br></br>
      <Form.Group controlId="imageUrl">
        <Form.Label>Image URL</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter image URL"
          required
          value={imageUrl}
          onChange={e => setImageUrl(e.target.value)}
        />
        <Form.Text className="text-muted">Please enter image URL</Form.Text>
      </Form.Group>
    </Col>
    <Col md={6}>
      <Form.Group controlId="productDescription">
        <Form.Label>Product Description</Form.Label>
        <Form.Control
          as="textarea"
          rows={16}
          cols={50}
          placeholder="Enter description"
          required
          value={description}
          onChange={e => setDescription(e.target.value)}
        />
        <Form.Text className="text-muted">Please enter product description</Form.Text>
      </Form.Group>
    </Col>
  </Row>
  <br></br>
  <div style={{ textAlign: 'center' }}>
    <Button variant="primary" type="submit" id="submitBtn">
      Submit
    </Button>
  </div>
</Form>
		    </div>
		  </div>
		</div>

	)
}